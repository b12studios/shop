<?php

use Faker\Generator as Faker;

$factory->define(App\Products::class, function (Faker $faker) {
    return [
        'qty_sold' => $faker->randomNumber(6),
        'product_name' => $faker->words(3),
        'product_description' => $faker->paragraph(3),
        'prinful_id' => $faker->randomNumber(6),
    ];
});
