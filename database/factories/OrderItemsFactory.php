<?php

use Faker\Generator as Faker;
use App\User;
use App\Products;
use App\Orders;

$factory->define(App\OrderItems::class, function (Faker $faker) {
    return [
        'order_id' => factory(Orders::class)->create()->id,
        'qty' => $faker->randomNumber(2),
        'customer_id' => factory(User::class)->create()->id,
        'product_id' => factory(Products::class)->create()->id,
    ];
});
