<?php

use App\User;
use Faker\Generator as Faker;

$factory->define(App\Orders::class, function (Faker $faker) use ($factory) {
    return [
        'order_id' => $faker->randomNumber(6),
        'total' => $faker->randomFloat(2,1,5),
        'customer_id' => factory(User::class)->create()->id,
        'printful_id' => $faker->randomNumber(6),
        ];
});
