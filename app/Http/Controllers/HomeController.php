<?php

namespace App\Http\Controllers;

use Printful\Exceptions\PrintfulApiException;
use Printful\Exceptions\PrintfulException;
use Printful\PrintfulApiClient;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $apiKey = '6owum7vw-kxme-hmq7:ohio-87fjns63zmrf';
        $pf = new PrintfulApiClient($apiKey);

        dd($pf->get('products'));
        return view('home');
    }
}
