<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuoteItems extends Model
{
    public function Quote()
    {
        return $this->belongsTo('App/Quote');
    }
}
