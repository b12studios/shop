<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{

    protected $primaryKey = 'order_id';


    public function User()
    {
        return $this->hasOne('App/User');
    }

    public function OrderItems()
    {
        return $this->hasMany('App/OrderItems');
    }
}
