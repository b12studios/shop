<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderItems extends Model
{
    public function Order()
    {
        return $this->belongsTo('App/Orders');
    }

    public function Product()
    {
        return $this->hasOne('App/Products');
    }
}
