<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quote extends Model
{
    public function Users()
    {
        return $this->hasOne('App/User');
    }

    public function QuoteItems()
    {
        return $this->hasMany('App/QuoteItems');
    }
}
